Comptage des voyageurs montants dans
 les trains Transilien par tranche horaire.
 Ces comptages manuels sont réalisés tous 
les trois à quatre ans et comptent le 
nombre de montants pour :

Chaque train d’une ligne Transilien 
et ce à chaque gare desservie
Chaque jour type (JOB, samedi et dimanche)
, hors travaux et vacances scolaires
Explication des libellés utilisés :

Nom gare : nom de la gare
Code gare : code UIC. Il est unique et permet l’identification de la gare
Type de jour : précise le jour du comptage: samedi, dimanche ou JOB (mardi ou jeudi)
Date de comptage : précise la date du comptage
Ligne : précise la ligne du train concernée lors du comptage car certaines gares sont desservies par plusieurs lignes. Dans ce cas, le détail est indiqué, comme dans l’exemple de la gare de Juvisy.
Tranche horaire : tranche horaire associé à l’horaire d’arrivée du train dans la gare
Montants : indique le nombre de montants dans les trains selon les critères ci-dessus
Axe :permettant d’identifier les axes d’une même ligne qui n’auraient pas été comptés à la même date


Pour une gare donnée, il peut y avoir plusieurs dates de comptages :
- Deux lignes qui passent par une même gare ne sont pas forcément comptées le même jour ;
- Et/ou il peut y avoir des journées de rattrapage si la première journée de comptage est incomplète.