---
title: "OBSERVATION DES COMPTAGES DE CHAQUE GARE TRANSILIEN DE 2014 A 2019"
author: "Romeo Agossou, Mariam Diallo"
date: "27/03/2020"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
```



## TABLE DES DONNEES

```{r}
file = "comptage-voyageurs-trains-transilien.csv"
if(!file.exists(file)){
  download.file("https://ressources.data.sncf.com/explore/dataset/comptage-voyageurs
-trains-transilien/download?format=csv&timezone=Europe/Berlin&use_labels_for_header=true",
	destfile=file)
}
read.csv(file, header = TRUE, sep=";")

```


```{r}
df <- read_delim("comptage-voyageurs-trains-transilien.csv",delim=";");
head(df)
```

## INTRODUCTION

Le présent jeu de données est un open data, une ressource de la Société nationale des chemins de fer français (SNCF) 
qui est l'entreprise ferroviaire publique française, officiellement créée par convention entre l'État et les compagnies 
de chemin de fer préexistantes. C'est un comptage des voyageurs montants dans les trains Transilien par tranche horaire. 
Ce comptage manuel est réalisé tous les trois à quatre ans et compte le nombre de montants pour :
- chaque train d'une ligne Transilien et à ce, à chaque gare desservie,
- chaque jour type (JOB, Samedi, Dimanche) hors travaux et vacances scolaires. Il est constitué de 9 attributs, à savoir : 
      Nom gare : nom de la gare
      Code gare : code UIC. Il est unique et permet l’identification de la gare
      Type de jour : précise le jour du comptage: samedi, dimanche ou JOB (mardi ou jeudi)
      Date de comptage : précise la date du comptage
      Ligne : précise la ligne du train concernée lors du comptage car certaines gares
              sont desservies par plusieurs lignes. Dans ce cas, le détail est indiqué, 
              comme dans l’exemple de la gare de Juvisy.
      Tranche horaire : tranche horaire associé à l’horaire d’arrivée du train dans la gare
      Montants : indique le nombre de montants dans les trains selon les critères ci-dessus
      Axe :permettant d’identifier les axes d’une même ligne qui n’auraient pas été comptés à la même date
Pour une gare donnée, il peut y avoir plusieurs dates de comptages :
    - Deux lignes qui passent par une même gare ne sont pas forcément comptées le même jour ;
    - Et/ou il peut y avoir des journées de rattrapage si la première journée de comptage est incomplète.
  
A travers, une analyse synoptique, plusieurs questions resortent. Au vue de tout, nous recensons :
  - quel est l'impact des horaires et les types de jours sur les voyages? 
  - Le montant total comptabilisé pour chaque gare peut nous permettre de 
  prouver la gare la plus fréquentée ou la ligne la plus fréquentée?
 
      - La ligne la plus frquentée ?
      - La gare la plus fréquentée ?

- affichage des montants pour chaque gare et par année : Exemple 
  d'affichage de 10 gares
- affichage par tranche horraire
- affichage par type de jour

  
  
## METHODOLOGIE

# 1- Procédures de nettoyage des données :
   Pour apporter des éléments de réponse, nous avons construit des sous-tables pour afficher : 
      - les montants pour chaque gare et par année
```{r}
an <- df %>% group_by(`Nom gare`, Year)%>%
              summarise(total=sum(Montants))
head(an)
```
      - la fréquence par année et par tranche horraire de chaque gare
```{r}
tra <- df %>% group_by(`Nom gare`, `Tranche horaire`, Year)%>%
              summarise(total=sum(Montants))
head(tra)
```     
       - la fréquence par année et par type de jour de chaque gare
       
```{r}
jou <- df %>% group_by(`Nom gare`, `Type jour`, Year)%>%
              summarise(total=sum(Montants))
head(jou)
```        
        - la fréquence par année et par type de chaque ligne
```{r}
li1 <- df %>% group_by(`Ligne`, `Type jour`, Year)%>%
              summarise(total=sum(Montants))
head(li1)
```          
         - la fréquence par année et par tranche horaire de chaque ligne
        
```{r}
li2 <- df %>% group_by(`Ligne`, `Tranche horaire`,  Year)%>%
              summarise(total=sum(Montants))
head(li2)
```          
        
  - les montants pour chaque ligne et par année
        
```{r}
li3 <- df %>% group_by(`Ligne`, Year)%>%
              summarise(total=sum(Montants))
head(li3)
```  
  
# 2- Flux de travail scientifique et Choix de représentation des données

                              
## Observation par type de jour
# comparativement aux tranches horaires, la ligne C est plus empruntée par les voyageurs suivant 
#les types de jour, mais encore les Mardi, Lundi et Samedi entre 2014 et 2018  
```{r}
hor <- df %>% group_by(`Nom gare`,`Type jour`, `Ligne`)%>%
      filter(`Type jour` != "Lundi" || `Type jour` != "Mercredi" || `Type jour` != "Vendredi") %>% 
       filter( `Nom gare` != "")%>%
            summarise(total=sum(Montants))

view(hor)
##geom_point(aes(x = `total`, y = `Ligne`, color = `Type jour`))
ggplot(hor) +
  geom_bar(aes(x = `Ligne`, fill = `Type jour`), 
           position = "dodge")
```
  

### Observation par tranche horaire
# Nous concluons que la ligne C indépendamment des types de jour, a enregistré plus de voyageurs 
# selon les tranches horaires, quelque soit la tranche horaire, les personnes ont voyagés plus sur 
#cette ligne de 2014 à 2018  
```{r}
hor1 <- df %>% group_by(`Nom gare`,`Ligne`,`Tranche horaire`)%>%
  filter(`Type jour` != "Lundi" || `Type jour` != "Mercredi" || `Type jour` != "Vendredi") %>% 
      filter(`Nom gare` != "")%>%
      summarise(total=sum(Montants))
  view(hor1)

ggplot(hor1) + 
  ##geom_point(aes(x = `Year`, y = `Ligne`, color = `Tranche horaire`))
  geom_bar(aes(x = `Ligne`, fill = `Tranche horaire`), 
           position = "dodge")



```
```{r}
f <- df %>% group_by(`Nom gare`)%>% 
  filter(`Ligne` != "M") %>% 
  filter(`Type jour` != "Lundi" || `Type jour` != "Mercredi" || `Type jour` != "Vendredi") %>% 
                        summarise(total1=sum(Montants))
view(f)
```

# découpage par gare, par type de jour, par ligne et année

# Observations des lignes par année : l'année 2018 compte plus de voyageurs que les autres années

```{r}

v <- df %>% group_by(`Nom gare`, `Type jour`, Year, Ligne)%>% 
   filter(`Ligne` != "M") %>% 
   filter(`Type jour` != "Lundi" || `Type jour` != "Mercredi" || `Type jour` != "Vendredi") %>% 
                        summarise(total=sum(Montants))
b = v[,c("Nom gare", "total")]
                      



                      
                              
##view(v)
ggplot(v) + 
   geom_point(aes(x = Year, y = `Ligne` , color = `total`, size = `Type jour`))

```



#### la gare qui a reçu plus de personne en 2016
```{r}
form = df %>% group_by(`Nom gare`,  Year) %>%
    summarise(total = sum(Montants)) %>%
    filter(Year=="2016")
view(form)
max(form$total)
  for (i in 1:102) {
    if(form$total[i] == max(form$total)) {
      gare <- form$`Nom gare`[i]
      return (gare)
    }
  }
print(gare)



```
  
  
## Analyse dans la programmation alphabetisée

Le découpage par gare, par type de jour, par ligne et année nous amène à faire les analyses ci-après :
Observations des lignes par année : les années 2018 et 2019 compte plus de voyageurs que les autres années
Nous concluons que la ligne C indépendamment des types de jour, a enregistré plus de voyageurs 
selon les tranches horaires, quelque soit la tranche horaire, les personnes ont voyagés plus sur 
cette ligne de 2014 à 2019.
Observation par type de jour s'explique par :
comparativement aux tranches horaires, la ligne C est plus empruntée par les voyageurs suivant 
les types de jour, mais encore les Mardi, Lundi et Samedi entre 2014 et 2019

## CONCLUSION
  Au vue de tout ce qui précède, nous retenons que, analyser une variable quantitative en fonction d'une variable qualitative est plus difficile à réaliser. 






## REFERENCES

Pour réaliser ce travail, nous avons utilisé comme ressources :
  - https://ressources.data.sncf.com/explore/dataset/comptage-voyageurs-trains-transilien/information/?sort=year
  






```{r}

```


```{r}

```

